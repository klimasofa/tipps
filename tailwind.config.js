const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./layouts/**/*.html"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Nunito", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        "klimasofa-green": {
          lighter: "oklch(92% 0.024 165)",
          light: "oklch(63% 0.126 165)",
          DEFAULT: "oklch(51.8% 0.103 165)", // #137b5a
          dark: "oklch(45% 0.087 165)",
          darker: "oklch(38% 0.074 165)",
        },
      },
    },
  },
};

---
title: "How to become a changemaker – Session 3"
date: 2022-04-11T11:51:37+02:00
draft: false
---

{{< button link="https://www.wwf.de/themen-projekte/landwirtschaft/ernaehrung-konsum/essen-wir-das-klima-auf" >}}
WWF: Essen wir das Klima auf?! {{< /button >}}

{{< button link="https://veganuary.com/de/" >}} Veganuary: Probier’s vegan.
Diesen Monat. {{< /button >}}

{{< button link="https://www.whatthehealthfilm.com/" >}} Film: What The Health
{{< /button >}}

{{< button link="http://tastethewaste.com/info/film" >}} Film: Taste The Waste
{{< /button >}}

{{< button link="https://toogoodtogo.de/de/" >}} Too Good To Go: Wir retten
Lebensmittel vor der Verschwendung {{< /button >}}

{{< button link="https://foodsharing.de/" >}} foodsharing Deutschland:
Lebensmittel teilen, statt wegwerfen {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=xFMZASJWz4w" >}} Youtube:
Jonathan Safran Foer – Lösungen für den Klimawandel {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=V1C-H-kZbqc" >}} Youtube: SWR
Ökochecker – Kakao und Schokolade: Fairtrade oder bio. Nur teuer oder wirklich
nachhaltig? {{< /button >}}

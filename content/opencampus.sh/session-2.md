---
title: "How to become a changemaker – Session 2"
date: 2022-04-11T11:51:37+02:00
draft: false
---

{{< button link="https://www.stockholmresilience.org/research/planetary-boundaries.html" >}}
Stockholm Resilience Centre: Planetary boundaries {{< /button >}}

{{< button link="https://www.overshootday.org/newsroom/country-overshoot-days/" >}}
Earth Overshoot Day: Country Overshoot Days {{< /button >}}

{{< button link="https://www.climate-and-hope.net/global-warming/carbon-clock" >}}
Climate and Hope: Carbon Clock {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=lpfhcer8nAo" >}} Youtube: Dr.
Thomas Bruhn – Das große Ganze und wir mittendrin. Denkweisen und
Geisteshaltungen für das Anthropozän {{< /button >}}

{{< button link="https://www.robinwood.de/oekostromreport" >}} Challenge: Robin
Wood – Ökostromreport 2020 {{< /button >}}

{{< button link="https://utopia.de/ratgeber/oekostrom-tarife-vergleich/" >}}
Challenge: Utopia.de – Ökostrom-Vergleich: Was diese 4 Tarife anderen voraus
haben {{< /button >}}

{{< button link="https://www.atmosfair.de/de/kompensieren/flug/" >}} Challenge:
Atmosfair – {{< CO2 >}}-Fußabdruck meines Flugs berechnen {{< /button >}}

{{< button link="https://uba.co2-rechner.de/de_DE/?do=reset" >}} Challenge:
Umweltbundesamt – Den eigenen {{< CO2 >}}-Fußabdruck berechnen {{< /button >}}

{{< button link="https://attenboroughfilm.com/" >}} Film: David Attenborough – A
Life on Our Planet {{< /button >}}

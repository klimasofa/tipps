---
title: "How to become a changemaker – Session 1"
date: 2022-04-11T11:51:37+02:00
draft: false
---

{{< button link="https://public.wmo.int/en/our-mandate/climate/wmo-statement-state-of-global-climate" >}}
WMO: The State of the Global Climate 2020 {{< /button >}}

{{< button link="https://www.ipcc.ch/" >}} IPCC – The Intergovernmental Panel on
Climate Change {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=3vhuFlVGBeI" >}} Youtube: Maja
Göpel (Scientists For Future) – Jung & Naiv: Folge 420 {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=ZE_TMzLCI_Y" >}} Youtube:
Katharina Van Bronswijk – How your climate emotions can save the world (TEDxHSG)
{{< /button >}}

{{< button link="https://filmsfortheearth.org/filme/cowspiracy/" >}} Challenge
Film gucken: Cowspiracy {{< /button >}}

{{< button link="https://filmsfortheearth.org/filme/the-true-cost/" >}}
Challenge Film gucken: The True Cost {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=Raqo-8gwO6s" >}} Challenge
Video gucken: Sean McCabe – Inequality and Climate Breakdown: The Survival
Paradox (TEDxDrogheda) {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=4UfpkRFPIJk" >}} Challenge
Video gucken: Alexander Gerst – Nachricht an meine Enkelkinder {{< /button >}}

{{< button link="https://www.wissenleben.de/2019/09/28/ich-schliesse-meine-augen/" >}}
Challenge Text lesen: Ich schließe meine Augen … – Ein Gedicht von Clara
Hanitzsch, 15 Jahre {{< /button >}}

{{< button link="https://www.isthishowyoufeel.com/this-is-how-scientists-feel.html" >}}
Challenge Text lesen: This is how scientists feel – Is this how you feel?
{{< /button >}}

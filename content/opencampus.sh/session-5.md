---
title: "How to become a changemaker – Session 5"
date: 2022-04-11T11:51:37+02:00
draft: false
---

{{< button link="https://www.fairlis.de/post/nachhaltige-textilien-und-welche-es-nicht-sind/" >}}
Nachhaltige Textilien {{< /button >}}

{{< button link="https://www.fairlis.de/post/nachhaltige-textilien-und-welche-es-nicht-sind/" >}}
Textilsiegel {{< /button >}}

{{< button link="https://www.klimasofa.org/bekleidungskonsum-wissen" >}}
Infothek des Klimasofas zum Bekleidungskonsum {{< /button >}}

{{< button link="https://www.ci-romero.de/produkt/dossier-fast-fashion/" >}}
CIR-Dossier: Fast Fashion – Eine Bilanz in 3 Teilen {{< /button >}}

{{< button link="https://fashionchangers.de/" >}} Fashion Changers
{{< /button >}}

{{< button link="https://fairwertung.de/" >}} FairWertung e. V. {{< /button >}}

{{< button link="https://fashionchecker.org/de/" >}} Fashion Checker
{{< /button >}}

{{< button link="https://www.fairwear.org/" >}} Fair Wear Foundation (Website
auf Englisch) {{< /button >}}

{{< button link="https://www.inkota.de/themen/kleidung-schuhe/schuhe-leder" >}}
INKOTA-netzwerk: Schuhe &amp; Leder. Global und schmutzig – Schuhproduktion
heute {{< /button >}}

{{< button link="https://greenwire.greenpeace.de/system/files/2019-04/s01951_greenpeace_report_konsumkollaps_fast_fashion.pdf">}}
Greenpeace: Konsumkollaps durch Fast Fashion (PDF) {{< /button >}}

{{< button link="https://www.greenpeace-hamburg.de/alltag-und-konsum/863/" >}}
Greenpeace Hamburg: Secondhand-Ratgeber für Hamburg {{< /button >}}

{{< button link="https://filmsfortheearth.org/" >}} Filme für die Erde
{{< /button >}}

{{< button link="https://talkslow.de/" >}} Talk Slow – Der Fair-Fashion-Podcast
{{< /button >}}

---
title: "How to become a changemaker – Session 4"
date: 2022-04-11T11:51:37+02:00
draft: false
---

{{< button link="https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming" >}}
Per Espen Stokes – How to transform apocalypse fatigue into action on global
warming (TED) {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=3tuaXaXJ02g" >}} YouTube:
Elisabeth Wehling – Die Macht der Sprachbilder. Politisches Framing
{{< /button >}}

{{< button link="https://nachhaltig-sein.info/lebensweise/rebound-und-backfire-effekte" >}}
Nachhaltig sein – Rebound- und Backfire-Effekte {{< /button >}}

{{< button link="https://perspective-daily.de/article/1841-was-wirklich-hilft-wenn-du-deine-gewohnheiten-aendern-willst/CsRs6CKB" >}}
Perspective Daily – Was wirklich hilft, wenn du deine Gewohnheiten ändern willst
{{< /button >}}

{{< button link="https://www.dragonsofinaction.com/" >}} The Dragons of Inaction
{{< /button >}}

{{< button link="https://www.planet-wissen.de/gesellschaft/psychologie/gewohnheiten/gewohnheiten-hirnforschung-100.html" >}}
Planet Wissen – Warum unser Gehirn Routinen liebt {{< /button >}}

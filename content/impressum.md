---
title: "Impressum"
date: 2021-10-31T20:31:07+01:00
draft: false
---

## Angaben gemäß § 5 TMG

Klimasofa e. V. \
℅ Gertz Gutsche Rümenapp Stadtentwicklung und Mobilität GbR \
Ruhrstraße 11 \
22761 Hamburg \
www.klimasofa.org

## Vertreten durch

Silke Quathamer, Carmen Schreib, Dr. Tobias Quathamer

## Kontakt

E-Mail: info@klimasofa.org \
Telefon: [+49 40 23205054](tel:+494023205054)

## Registereintrag

Amtsgericht Hamburg VR 24177

## Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV

Klimasofa e. V. \
Vertretungsberechtigt: Silke Quathamer, Carmen Schreib, Dr. Tobias Quathamer \
℅ Gertz Gutsche Rümenapp Stadtentwicklung und Mobilität GbR \
Ruhrstraße 11 \
22761 Hamburg

## Widerspruch Werbe-Mails

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten
zur Übersendung von nicht ausdrücklich angeforderter Werbung und
Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten
behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten
Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.

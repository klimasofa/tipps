---
title:
  "Corona, Krieg und Klimakrise – Wie wir aktiv und hoffnungsvoll bleiben können"
date: 2022-09-21T17:23:10+02:00
draft: false
---

{{< button link="https://www.klimasofa.org/infothek" >}} Infothek des Klimasofas
{{< /button >}}

{{< button link="https://www.youtube.com/watch?v=eGgUGdIttMo" >}} Youtube: Wenn
alles so furchtbar kompliziert ist: Ansätze zum Nachhaltigkeitshandeln in einer
komplexen Welt – Prof. Dr. Henning Pätzold {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=ZE_TMzLCI_Y" >}} Youtube: How
your climate emotions can save the world – Katharina van Bronswijk
{{< /button >}}

{{< button link="https://www.youtube.com/watch?v=lpfhcer8nAo" >}} Youtube: Das
große Ganze – und wir mittendrin. Denkweisen und Geisteshaltungen für das
Anthropozän – Dr. Thomas Bruhn {{< /button >}}

{{< button link="https://perspective-daily.de/article/feed/#/newest?page=1" >}}
Perspective Daily – Artikelübersicht {{< /button >}}

{{< button link="https://goodnews.eu/" >}} Good News {{< /button >}}

{{< button link="https://maren-urner.com/buecher" >}} Bücher von Prof. Dr. Maren
Urner {{< /button >}}

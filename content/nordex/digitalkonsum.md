---
title: "Digitalkonsum"
date: 2022-09-21T17:17:33+02:00
draft: false
---

{{< button link="https://www.klimasofa.org/infothek" >}} Infothek des Klimasofas
{{< /button >}}

{{< button link="https://www.digitalcarbonfootprint.eu/" >}} Digitaler
{{< CO2 >}}-Fußabdruck {{< /button >}}

{{< button link="https://www.oekom.de/buch/was-bits-und-baeume-verbindet-9783962381493" >}}
Buch: Was Bits und Bäume verbindet {{< /button >}}

{{< button link="https://www.oekom.de/buch/smarte-gruene-welt-9783962380205" >}}
Buch: Smarte grüne Welt? {{< /button >}}

{{< button link="https://www.nachhaltige-digitalisierung.de/" >}} Website:
Nachhaltige Digitalisierung {{< /button >}}

{{< button link="https://vimeo.com/148891164" >}} Film: Kaufen für die Müllhalde
– Geplante Obsoleszenz {{< /button >}}

{{< button link="http://www.behindthescreen.at/" >}} Film: BEHIND THE SCREEN –
Das Leben meines Computers {{< /button >}}

{{< button link="http://www.the-congo-tribunal.com/" >}} Film: Das Kongo
Tribunal {{< /button >}}

{{< button link="https://www.thesocialdilemma.com/de/the-film/" >}} Film: Das
Dilemma mit den sozialen Medien {{< /button >}}

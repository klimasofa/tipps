---
title: "Tipps vom Klimasofa"
date: 2021-10-30T21:07:44+02:00
draft: false
---

Du kannst die Tipps für das jeweilige Thema mit dem Link aufrufen, den du bei
der Veranstaltung von uns bekommen hast.

Für weitere Informationen kannst du auch die
[Infothek auf unserer Website](https://www.klimasofa.org/infothek) ansehen.

---
title: "Datenschutz"
date: 2021-10-31T20:56:02+01:00
draft: false
---

Auf dieser Website werden keinerlei personenbezogene Daten erhoben oder
gespeichert. Wir werten nicht aus, welche Links angeklickt werden.

## Cookies

Diese Website verwendet keine Cookies.

## Server-Log-Files

Es werden weder von uns noch von unserem Provider Server-Log-Files erstellt.

## Widerspruch Werbe-Mails

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten
zur Übersendung von nicht ausdrücklich angeforderter Werbung und
Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten
behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten
Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.

---
title: "Fashion For Future"
date: 2022-03-21T11:11:44+01:00
draft: false
---

{{< button link="https://www.fashionrevolution.org/" >}} Fashion Revolution
(Activism Movement/NGO) {{</ button >}}

{{< button link="https://www.fairwear.org/" >}} Fair Wear Foundation (NGO)
{{</ button >}}

{{< button link="https://cleanclothes.org/" >}} Clean Clothes Campaign (NGO)
{{</ button >}}

{{< button link="https://hej-support.org/" >}} Health and Environment Justice
Support (NGO) {{</ button >}}

{{< button link="https://www.youtube.com/watch?v=cduGLWhm1LM" >}} You are what
you wear: Christina Dean at TEDxHKBU (YouTube) {{</ button >}}

{{< button link="https://truecostmovie.com/" >}} The True Cost (Movie)
{{</ button >}}

{{< button link="https://www.youtube.com/watch?v=CSUmo-40pqA" >}} Two adults,
two kids, zero waste | Bea Johnson | TEDxFoggyBottom (YouTube) {{</ button >}}

---
title: "Klimafair leben"
date: 2024-11-01T12:12:12+01:00
draft: false
---

{{< button link="https://www.boell.de/de/fleischatlas" >}} Fleischatlas
{{< /button >}}

{{< button link="https://www.zugutfuerdietonne.de/" >}} Lebensmittelrettung
{{< /button >}}

{{< button link="https://veggie-specials.com/" >}} Günstige vegane
Bio-Lebensmittel {{< /button >}}

{{< button link="https://www.bund.net/massentierhaltung/haltungskennzeichnung/bio-siegel/" >}}
Biosiegel {{< /button >}}

{{< button link="https://waterfootprint.org/en/" >}} Wasserfußabdruck
{{< /button >}}

{{< button link="https://www.wwf.de/themen-projekte/landwirtschaft/ernaehrung-konsum/essen-wir-das-klima-auf" >}}
WWF: Essen wir das Klima auf? {{< /button >}}

{{< button link="https://veganuary.com/de/" >}} Veganuary: Probier’s vegan.
Diesen Monat. {{< /button >}}

{{< button link="https://www.zuckerjagdwurst.com/de" >}} Zucker & Jagdwurst,
veganer Foodblog {{< /button >}}

{{< button link="https://www.blitzrechner.de/veggie/" >}} Fleischlos-Bilanz: Der
Veggie-Rechner {{< /button >}}

{{< button link="https://www.stockholmresilience.org/research/planetary-boundaries.html" >}}
Stockholm Resilience Centre: Planetary boundaries {{< /button >}}

{{< button link="https://www.whatthehealthfilm.com/" >}} Film: What The Health
{{< /button >}}

{{< button link="https://toogoodtogo.de/de/" >}} Too Good To Go: Wir retten
Lebensmittel vor der Verschwendung {{< /button >}}

{{< button link="https://foodsharing.de/" >}} foodsharing Deutschland:
Lebensmittel teilen, statt wegwerfen {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=xFMZASJWz4w" >}} Youtube:
Jonathan Safran Foer – Lösungen für den Klimawandel {{< /button >}}

{{< button link="https://www.youtube.com/watch?v=V1C-H-kZbqc" >}} Youtube: SWR
Ökochecker – Kakao und Schokolade: Fairtrade oder bio. Nur teuer oder wirklich
nachhaltig? {{< /button >}}

---
title: "Finanzen"
date: 2021-11-04T15:19:28+01:00
draft: false
---

{{< button link="https://www.fairfinanceguide.de/" >}} Fair Finance Guide
Deutschland {{< /button >}}

{{< button link="https://www.geld-bewegt.de/" >}} Geld bewegt – Anlegen für
Mensch und Umwelt {{< /button >}}

{{< button link="https://venga-ev.org/" >}} VenGa e. V. {{< /button >}}

{{< button link="https://www.ecoreporter.de/" >}} ECOreporter {{< /button >}}

{{< button link="https://www.urgewald.org/bankwechsel" >}} Urgewald –
Bankwechsel {{< /button >}}

{{< button link="https://www.finance-4future.de/podcast/" >}} Finance 4Future –
Podcast für nachhaltige Geldanlagen {{< /button >}}

{{< button link="https://www.test.de/Ethisch-oekologische-Fonds-So-legen-Sie-sauber-an-4741500-0/" >}}
Stiftung Warentest – Nachhaltige Fonds und ETF {{< /button >}}

{{< button link="https://fng-siegel.org/" >}} FNG-Siegel – Qualitätsstandard für
Nachhaltige Geldanlagen {{< /button >}}

{{< button link="https://www.cleanvest.org/de/" >}} CLEANVEST –
Vergleichsplattform für nachhaltige Anlageprodukte {{< /button >}}

{{< button link="https://www.gls-fonds.de/" >}} Nachhaltige Fonds der GLS-Bank
{{< /button >}}

{{< button link="https://www.oekoworld.com/privatpersonen/investmentfonds/oekoworld-oekovision-classic/portrait" >}}
ÖkoVision: Nachhaltiger Fonds der Ökoworld AG {{< /button >}}
